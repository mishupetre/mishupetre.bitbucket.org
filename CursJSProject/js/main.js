'use strict';

// make sure our code runs after the DOM is loaded
document.addEventListener('DOMContentLoaded', function() {

  // self invoked function that triggers behavior for the gallery
  (function gallery() {
    var galleryContainer = document.querySelector('.gallery-container');
    var numberOfImages = document.querySelectorAll('.gallery-img').length;

    var prevBtn = document.querySelector('.prev');
    prevBtn.addEventListener('click', function() {
      // inserting the last image in the gallery before the first one
      galleryContainer.insertBefore(document.querySelectorAll('.gallery-img')[numberOfImages - 1],
        document.querySelectorAll('.gallery-img')[0]);
    });

    var nextBtn = document.querySelector('.next');
    nextBtn.addEventListener('click', function() {
      // appending the first image in the gallery as the last child of the container
      galleryContainer.appendChild(document.querySelectorAll('.gallery-img')[0]);
    });
  })();

  // self invoked function that triggers behavior for the tabs
  (function simpleTabs() {
    var tabNav = document.querySelector('.tabs-nav');
    var tabPanels = document.querySelectorAll('.tab-panel');
    var tabNavLinks = document.querySelectorAll('.tabs-nav-link');

    // adding an event listener on the parent so we don't have to add one on each tab nav link
    tabNav.addEventListener('click', changeTab);

    function changeTab(ev) {

      // verify if the clicked element is not the same as the event listener target, because  
      // we only want this to trigger on children of the target, not the target itself
      if (ev.target !== ev.currentTarget) {

        // remove all styles that make an item show as active
        for (var i = 0; i < tabPanels.length; ++i) {
          tabPanels[i].style.display = 'none';
          tabNavLinks[i].classList.remove('active');
        }

        // set the active class on the clicked tab nav link
        ev.target.classList.add('active');

        // get the value of the href attribute without the # at the beginning
        var targetPanel = ev.target.getAttribute('href').substr(1);
        document.getElementById(targetPanel).style.display = 'block';
      }

      // stop the event bubbling
      ev.stopPropagation();
    }
  })();

  //
  (function formValidation() {
    var form = document.getElementById('formValidation');
    var name = document.getElementById('name');
    var errorName = document.querySelector('.form-error.name');
    var email = document.getElementById('email');
    var errorEmail = document.querySelector('.form-error.email');
    var password = document.getElementById('password');
    var errorPassword = document.querySelector('.form-error.password');
    var passwordConf = document.getElementById('passwordConf');
    var errorPasswordConf = document.querySelector('.form-error.passwordConf');

    function inputInvalid(elem, errorElem) {
      errorElem.style.display = 'block';
      elem.style.borderColor = 'red';
    }

    function inputValid(elem, errorElem) {
      errorElem.style.display = 'none';
      elem.style.borderColor = 'green';
    }

    name.addEventListener('input', function() {

      // name input validation
      if (name.value.length < 4) {
        inputInvalid(name, errorName);
      } else {
        inputValid(name, errorName);
      }
    });

    email.addEventListener('input', function() {

      // email input validation
      if (email.value.indexOf('@') === -1 ||
        email.value.indexOf('@') !== email.value.lastIndexOf('@') ||
        email.value.indexOf('@') < 3 ||
        email.value.lastIndexOf('.') > (email.value.length - 3) ||
        email.value.indexOf('@') >= (email.value.lastIndexOf('.') - 3)
      ) {
        inputInvalid(email, errorEmail);
      } else {
        inputValid(email, errorEmail);
      }
    });

    password.addEventListener('input', function() {

      // password input validation
      if (password.value.length < 8) {
        inputInvalid(password, errorPassword);
      } else {
        inputValid(password, errorPassword);
      }
    });

    passwordConf.addEventListener('input', function() {

      // password confirmation input validation
      if (passwordConf.value !== password.value) {
        inputInvalid(passwordConf, errorPasswordConf);
      } else {
        inputValid(passwordConf, errorPasswordConf);
      }
    });

    form.addEventListener('submit', function() {
      if (name.value && email.value && password.value && passwordConf.value) {
        alert('Congratulations ' + form.name.value + ', your form has been submitted.');
      } else {
        alert('Please fill in all the form fields!');
      }
    });
  })();
});